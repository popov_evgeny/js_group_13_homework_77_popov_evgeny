const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', (req, res) => {
  const messages = db.getMessages();
  return res.send(messages);
});


router.post('/', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.message){
      return res.status(400).send({message: 'Error message!'})
    } else {
      const message = {
        author: req.body.author,
        message: req.body.message
      };

      if (req.file) {
        message.image = req.file.filename;
      }

      await db.addMessage(message);
      return res.send(message);
    }
  } catch (e) {
    next(e);
  }
});

module.exports = router;