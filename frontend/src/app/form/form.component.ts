import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MessagesService } from '../services/messages.service';
import { intMessage } from '../models/message.model';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.sass']
})
export class FormComponent implements OnInit, OnDestroy{
  @ViewChild('form') form!: NgForm;
  loadingSubscription!: Subscription;
  loading = false;

  constructor(private messagesService: MessagesService) {}

  ngOnInit() {
    this.loadingSubscription = this.messagesService.postLoading.subscribe( postLoading => {
      this.loading = postLoading;
    });
  }

  onSubmit() {
    const message: intMessage = this.form.value;
    this.messagesService.creatMessage(message);
    this.form.resetForm();
  }

  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
  }
}
