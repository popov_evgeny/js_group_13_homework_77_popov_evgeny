import { Component, OnDestroy, OnInit } from '@angular/core';
import { Message } from '../models/message.model';
import { MessagesService } from '../services/messages.service';
import { Subscription } from 'rxjs';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit, OnDestroy {
  arrMessages: Message[] = [];
  arrMessagesSubscription!: Subscription;
  apiUrl = environment.apiUrl;
  loadingSubscription!: Subscription;
  loading = false;
  page = 1;

  lengthPages!: number;

  constructor(private messagesService: MessagesService) {}

  ngOnInit(): void {
    this.loadingSubscription = this.messagesService.getLoading.subscribe( getLoading => {
      this.loading = getLoading;
    });
    this.arrMessagesSubscription = this.messagesService.changeArrMessages.subscribe( array => {
      this.arrMessages = array;
      this.lengthPages = this.arrMessages.length;
    });
    this.messagesService.getMessages();
  }
  ngOnDestroy() {
    this.loadingSubscription.unsubscribe();
    this.arrMessagesSubscription.unsubscribe();
  }
}
