export class Message {
  constructor(
    public id: string,
    public author: string,
    public message: string,
    public image: string,
  ) {}
}

export interface intMessage {
  author: string;
  message: string;
  image: File | null;
}
