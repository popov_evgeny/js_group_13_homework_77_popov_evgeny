import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { intMessage, Message } from '../models/message.model';
import { map, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  private arrMessages: Message[] = [];
  changeArrMessages = new Subject<Message[]>();
  postLoading = new Subject<boolean>();
  getLoading = new Subject<boolean>();

  constructor(private http: HttpClient) {}

  getMessages() {
    this.getLoading.next(true);
    this.http.get<Message[]>('http://localhost:8000/chat').pipe(map( response => {
      return response.map(messageData => {
        return new Message(
          messageData.id,
          messageData.author,
          messageData.message,
          messageData.image
        )});
    })).subscribe( {
      next:(array => {
        this.arrMessages = array;
        this.changeArrMessages.next(this.arrMessages.slice().reverse());
        this.getLoading.next(false);
      }),
      error:(() => {
        this.getLoading.next(false);
      })
  }
    );
  }

  creatMessage(message: intMessage) {
    this.postLoading.next(true);
    const formData = new FormData();
    formData.append('author', message.author);
    formData.append('message', message.message);
    if (message.image) {
      formData.append('image', message.image);
    }

    this.http.post<intMessage>('http://localhost:8000/chat', formData).subscribe({
    next:() => {
      this.getMessages();
      this.postLoading.next(false);
    },
      error:(() => {
        this.postLoading.next(false);
      })
    });
  }

}
